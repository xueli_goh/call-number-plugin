module.exports = {  
  callNumber: function(successCallback, errorCallback, number) {
    cordova.exec(successCallback, errorCallback, 'CallNumberPlugin', 'callNumber', [number]);
  }
};